
package collections;

import java.util.Objects;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 21 mar 2023
 */
public class Persona{

	int id;
	String nom;
	public Persona(int id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@Override
	public String toString() {
		return "Persona [id=" + id + ", nom=" + nom + "]";
	}
	
	
	
	
	
}
