package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 16 mar 2023
 */
public class Lists {

	static ArrayList<String> names = new ArrayList<>();
	static LinkedList<String> linkedNames = new LinkedList<>();
	
	public static void main(String[] args) {
		String nom1 = "Alvaro";
		String nom2 = "Pol";
		String nom3 = "Jaume";
		String nom4 = "Alvaro";
		names.add(nom1);
		names.add(nom1);
		names.add(nom1);
		names.add(nom1);
		names.add(nom1);
		System.out.println(names);
		Collections.sort(names);
		
//		for (String string : names) {
//			names.remove(0);	
//		}
		
//		Iterator<String> it = names.iterator();
//		while (it.hasNext()) {
//			it.remove();
//		}
		
		System.out.println(names);
		
		HashMap<Integer, String> mapa = new HashMap<>();
		mapa.put(4, "num 4");
		mapa.put(2, "num 2");
		mapa.put(3, "num 3");
		
		for (String id : mapa.values()) {
			System.out.println(id);			
		}
		Map< Integer, String> map = new HashMap<Integer, String>();
	}
}
