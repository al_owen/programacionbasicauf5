
package collections;

import java.util.TreeSet;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 21 mar 2023
 */
public class TreeSets {

	public static void main(String[] args) {
		
		//creo un comparator para poder comparar cada elemento de manera
		//que al ingresar el nuevo objeto, verifica el identificador y lo ordena 
		//en base a ese id
		TreeSet<Persona> people = new TreeSet<>(new PersonaComparator());
		people.add(new Persona(5, "Pepe"));
		people.add(new Persona(3, "Adrian"));
		people.add(new Persona(1, "juan"));
		
		//se puede borrar objetos enviado un objeto con el id que queremos eliminar
		people.remove(new Persona(1, ""));
		
		for (Persona persona : people) {
			System.out.println(persona);
			
		}
		
		
	}
}
