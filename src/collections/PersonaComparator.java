package collections;

import java.util.Comparator;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 21 mar 2023
 */
public class PersonaComparator implements Comparator<Persona> {

	/**
	 * Crea un comparador que mira el id de persona a la hora de compararlo
	 * devuelve 0 si ambos valores son iguales; -1 si p1 es menor que p2 y 
	 * 1 si p1 es mayor que p2, esto determina el peso del valor
	 */
	@Override
	public int compare(Persona p1, Persona p2) {
		if (p1.getId() == p2.getId()) {
			return 0;
		}else if (p1.getId() < p2.getId()) {
			return -1;
		}else {
			return 1;
		}
		
		//return Integer.compare(p1.getId(), p2.getId());
	}

}
