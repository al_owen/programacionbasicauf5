package fechasYhoras;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalField;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 23 mar 2023
 */
public class Fechas {

	public static void main(String[] args) {
		//crear una fecha a partir de numeros
		LocalDate unaFecha = LocalDate.of(2021, 05, 25);
		System.out.println(unaFecha);
		
		//formatar la fecha
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String f = unaFecha.format(dtf);
		System.out.println("Fecha con formato "+ f);
		
		//convertir una string en una fecha
		LocalDate parsed = LocalDate.parse("08/08/2028", dtf);
		System.out.println(parsed);
		
		//mostrar partes especificas de la fecha
		System.out.println("año " +parsed.getYear());
		System.out.println("mes " +parsed.getMonth());
		System.out.println("dia " +parsed.getDayOfMonth());
		
		//obtener la fecha de actual
		LocalDate hoy = LocalDate.now();
		//si ponemos MMMM nos muestra la fecha en Sring
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy");
		String text = hoy.format(formatter);
		System.out.println(text);
		
		//compara que fecha es mayor
		System.out.println("compare "+hoy.compareTo(unaFecha));
		if (hoy.compareTo(unaFecha) == 0) {
			System.out.println("La fecha es la misma");
		}else if (hoy.compareTo(unaFecha) > 0) {
			System.out.println("la primera fecha es mayor");
		}else if (hoy.compareTo(unaFecha) < 0) {
			System.out.println("la primera fecha es menor");
		}
		
		//ver los dias de diferencia que hay entre una fecha y otra
		System.out.println("Diferencia "+Math.abs(ChronoUnit.YEARS.between(hoy, unaFecha)));
		
		
		//agregar dias, meses y años
		LocalDate siguienteSemana = hoy.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
		System.out.println(siguienteSemana);
		
		LocalDate dentroDeTresDias = hoy.plusDays(17);
		System.out.println(dentroDeTresDias);
	}
}
