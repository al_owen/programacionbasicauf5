package internacionalizacion;

import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Locale.Category;
import java.util.ResourceBundle;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 27 mar 2023
 */
public class MenuInternacional {

public static void main(String[] args) {
		
		textDisplay();
		numberFormat();
	}
	
	public static void textDisplay() {
		
		Locale lDisplay = Locale.getDefault(Category.DISPLAY);  //nos devuelve la informacion del idioma y el pais de nuestra configuraci�n
		//Locale lDisplay = new Locale("es", "ES");  //se puede crear un locale pasandole el idioma y pais, en ese orden
		//Locale lDisplay = new Locale("ca", "ES");
		System.out.println(lDisplay);
		
		//nos carga el documento '.properties' que le pasemos, sin embargo no debemos de especificar la extension.
		//ResourceBundle text = ResourceBundle.getBundle("Texts"); //si no le pasamos un locale, por defecto buscara nuestro idioma, si no lo encuentra devolvera el texto por defecto
		ResourceBundle text = ResourceBundle.getBundle("Texts", lDisplay); //es posible pasarle el idioma manualmente
		
		System.out.println(text.getString("exit")); //para obtener los valores le debemos pasar la clave que sera igual en todos los idiomas
		System.out.println(text.getString("0001"));
	}
	
	public static void numberFormat() {
		//Locale lFormat = Locale.getDefault(Category.FORMAT);
		Locale lFormat = new Locale("en", "US");
		System.out.println(lFormat);
		
		//formateadores
		DateTimeFormatter dFormater = DateTimeFormatter.ofPattern( "dd/MM/yyyy", new Locale("es","ES"));
		NumberFormat nFormatter = NumberFormat.getNumberInstance(lFormat); //podemos obtener el formato de los numeros del pais especificado en el locale
		NumberFormat cFormatter = NumberFormat.getCurrencyInstance(lFormat); //podemos obtener el formato monetario del pais especificado en el locale
		
		int cantidad = 32154984;
		double precio = 15.65;
		double importe = cantidad * precio;
		
		//para modificar el formato de los numeros le pasamos el numero al formateador
		System.out.println("Cantidad: "+nFormatter.format(cantidad)); 
		System.out.println("Precio: "+cFormatter.format(precio));
		System.out.println("Import: "+cFormatter.format(importe));
		
	}
	
}
