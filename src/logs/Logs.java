/**
 * 
 */
package logs;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.print.attribute.standard.Severity;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 9 mar 2023
 */
public class Logs {

	public static void main(String[] args) throws IOException {
		Logger log = Logger.getLogger("logs");
		FileHandler fh = new FileHandler("res/log.txt", true);
		fh.setFormatter(new SimpleFormatter());
		log.addHandler(fh);
		log.setLevel(Level.ALL);
		log.info("inicio");
		try (Scanner sc = new Scanner(System.in)){
			System.out.println(sc.nextInt());
			log.fine("Fue todo bien");
			Errores.connect();
			///aqui tiene que ir el menu
		} catch (InputMismatchException ex) {
			log.log(Level.SEVERE, "Error al pedir el valor, el tipo no coincide!", ex);
		}
	}
}
