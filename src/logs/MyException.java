/**
 * 
 */
package logs;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF5 
 * 13 mar 2023
 */
public class MyException extends Exception{
	
	public MyException() {
		super();
	}

	public MyException(String msg) {
		super(msg);
	}
	
}
