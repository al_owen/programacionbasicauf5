package logs;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF5 9 mar 2023
 */
public class Errores {

	public static void main(String[] args) {
		
		try {
			System.out.println(check(1));
		} catch (MyException e) {
			System.out.println(e.getMessage());
		}catch(InputMismatchException ie){
			
		} catch (Exception e) {
			System.out.println("pum");
		}
		
	}
	
	
	public static String check(int num)throws MyException{
		if (num == 1) {
			throw new MyException("Que me quedo sin comer...");
		}
		return "yay";
	}
	
	
	public static void connect() {
		Logger log = Logger.getLogger("logs");
		try (Scanner sc = new Scanner(System.in)){
			Integer.parseInt(sc.nextLine());
		} catch (NumberFormatException e) {
			Logger.getLogger("logs").log(Level.SEVERE, "El número es incorrecto", e);
		}

	}
}
